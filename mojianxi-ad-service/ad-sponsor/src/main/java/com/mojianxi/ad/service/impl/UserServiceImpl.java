package com.mojianxi.ad.service.impl;

import com.mojianxi.ad.constant.Constants;
import com.mojianxi.ad.dao.AdUserRepository;
import com.mojianxi.ad.entity.AdUser;
import com.mojianxi.ad.exception.AdException;
import com.mojianxi.ad.service.IUserService;
import com.mojianxi.ad.utils.CommonUtils;
import com.mojianxi.ad.vo.CreateUserRequest;
import com.mojianxi.ad.vo.CreateUserResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * @Description TODO
 * @Author:liuxiaodong
 * @Date 2019/11/5 22:08
 * @Company dyld
 */
@Slf4j
@Service
public class UserServiceImpl implements IUserService {
    private final AdUserRepository userRepository;
    @Autowired
    public UserServiceImpl(AdUserRepository adUserRepository) {
        this.userRepository = adUserRepository;
    }

    @Override
    @Transactional
    public CreateUserResponse createUser(CreateUserRequest request) throws AdException {
        if(!request.validate()){
            throw new AdException(Constants.ErrorMsg.REQUEST_PARAM_ERROR);
        }
        AdUser oldUser=userRepository.findByUsername(request.getUsername());
        if(oldUser!=null){
            throw new AdException(Constants.ErrorMsg.SAME_NAME_ERROR);
        }
        AdUser newUser=userRepository.save(new AdUser(request.getUsername(), CommonUtils.md5(request.getUsername())));
        return new CreateUserResponse(newUser.getId(),newUser.getUsername(),newUser.getToken(),newUser.getCreateTime(),newUser.getUpdateTime());
    }
}

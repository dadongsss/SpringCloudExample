package com.mojianxi.ad.utils;


import com.mojianxi.ad.exception.AdException;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.lang.time.DateUtils;

import java.util.Date;

/**
 * @Description TODO
 * @Author:liuxiaodong
 * @Date 2019/11/5 22:17
 * @Company dyld
 */
public class CommonUtils {
    private static String[] parsePatterns={
        "yyyy-MM-dd","yyyy/MM/dd","yyyy.MM.dd"
    };
    public static String md5(String value){
        return DigestUtils.md5Hex(value).toUpperCase();
    }
    public static Date parseStringDate(String dateString)throws AdException{
        try {
            return DateUtils.parseDate(dateString,parsePatterns);
        }catch (Exception e){
            throw new AdException(e.getMessage());
        }
    }
}

package com.mojianxi.ad.service;

import com.mojianxi.ad.exception.AdException;
import com.mojianxi.ad.vo.CreateUserRequest;
import com.mojianxi.ad.vo.CreateUserResponse;

/**
 * @Description TODO
 * @Author:liuxiaodong
 * @Date 2019/11/5 22:01
 * @Company dyld
 */
public interface IUserService {
    /*创建用户*/
    CreateUserResponse createUser(CreateUserRequest request) throws AdException;
}

package com.mojianxi.ad.service.impl;

import com.mojianxi.ad.constant.CommonStatus;
import com.mojianxi.ad.constant.Constants;
import com.mojianxi.ad.dao.AdPlanRepository;
import com.mojianxi.ad.dao.AdUserRepository;
import com.mojianxi.ad.entity.AdPlan;
import com.mojianxi.ad.entity.AdUser;
import com.mojianxi.ad.exception.AdException;
import com.mojianxi.ad.service.IAdPlanService;
import com.mojianxi.ad.utils.CommonUtils;
import com.mojianxi.ad.vo.AdPlanGetRequest;
import com.mojianxi.ad.vo.AdPlanRequest;
import com.mojianxi.ad.vo.AdPlanResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;
import java.util.Optional;

/**
 * @Description TODO
 * @Author:liuxiaodong
 * @Date 2019/11/5 22:41
 * @Company dyld
 */
@Service
public class AdPlanServiceImpl implements IAdPlanService {
    private final AdUserRepository userRepository;
    private final AdPlanRepository planRepository;

    @Autowired
    public AdPlanServiceImpl(AdUserRepository userRepository, AdPlanRepository planRepository) {
        this.userRepository = userRepository;
        this.planRepository = planRepository;
    }

    @Override
    @Transactional
    public AdPlanResponse createAdPlan(AdPlanRequest request) throws AdException {
        if(!request.createValidate()){
            throw new AdException(Constants.ErrorMsg.REQUEST_PARAM_ERROR);
        }
        //确保关联的user是存在的
        Optional<AdUser> adUser=userRepository.findById(request.getUserId());
        if(!adUser.isPresent()){
            throw new AdException(Constants.ErrorMsg.CAN_NOT_FIND_RECORD);
        }
        AdPlan oldPlan=planRepository.findByUserIdAndPlanName(request.getUserId(),request.getPlanName());
        if(oldPlan!=null){
            throw new AdException(Constants.ErrorMsg.SAME_NAME_PLAN_ERROR);
        }
        AdPlan newAdPlan=planRepository.save(new AdPlan(request.getUserId(),request.getPlanName(), CommonUtils.parseStringDate(request.getStartDate()),
                CommonUtils.parseStringDate(request.getEndDate())));
        return new AdPlanResponse(newAdPlan.getId(),newAdPlan.getPlanName());
    }

    @Override
    public List<AdPlan> getAdPlansByIds(AdPlanGetRequest request) throws AdException {
        if(!request.validate()){
            throw new AdException(Constants.ErrorMsg.REQUEST_PARAM_ERROR);
        }
        return planRepository.findAllByIdInAndUserId(request.getIds(),request.getUserId());
    }

    @Override
    public AdPlanResponse updateAdPlan(AdPlanRequest request) throws AdException {
        if(!request.updateValidate()){
            throw new AdException(Constants.ErrorMsg.REQUEST_PARAM_ERROR);
        }
        AdPlan plan=planRepository.findByIdAndUserId(request.getId(),request.getUserId());
        if(plan==null){
            throw new AdException(Constants.ErrorMsg.CAN_NOT_FIND_RECORD);
        }
        if(request.getPlanName()!=null){
            plan.setPlanName(request.getPlanName());
        }
        if(request.getStartDate()!=null){
            plan.setStartDate(CommonUtils.parseStringDate(request.getStartDate()));
        }
        if(request.getEndDate()!=null){
            plan.setEndDate(CommonUtils.parseStringDate(request.getEndDate()));
        }
        plan.setUpdateTime(new Date());
        plan=planRepository.save(plan);
        return new AdPlanResponse(plan.getId(),plan.getPlanName());
    }

    @Override
    @Transactional
    public void deleteAdPlan(AdPlanRequest request) throws AdException {
        if(!request.deleteValidate()){
            throw new AdException(Constants.ErrorMsg.REQUEST_PARAM_ERROR);
        }
        AdPlan plan=planRepository.findByIdAndUserId(request.getId(),request.getUserId());
        if(plan==null){
            throw new AdException(Constants.ErrorMsg.CAN_NOT_FIND_RECORD);
        }
        plan.setPlanStatus(CommonStatus.INVALID.getStatus());
        plan.setUpdateTime(new Date());
        planRepository.save(plan);
    }
}

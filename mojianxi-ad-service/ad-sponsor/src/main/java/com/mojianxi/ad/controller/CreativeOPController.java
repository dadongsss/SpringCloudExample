package com.mojianxi.ad.controller;

import com.alibaba.fastjson.JSON;
import com.mojianxi.ad.service.ICreativeService;
import com.mojianxi.ad.vo.CreativeRequest;
import com.mojianxi.ad.vo.CreativeResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Description TODO
 * @Author:liuxiaodong
 * @Date 2019/11/6 0:50
 * @Company dyld
 */
@Slf4j
@RestController
public class CreativeOPController {
    private final ICreativeService creativeService;

    @Autowired
    public CreativeOPController(ICreativeService creativeService) {
        this.creativeService = creativeService;
    }

    @PostMapping("/create/creative")
    public CreativeResponse createCreative(
            @RequestBody CreativeRequest request
    ) {
        log.info("ad-sponsor: createCreative -> {}",
                JSON.toJSONString(request));
        return creativeService.createCreative(request);
    }
}

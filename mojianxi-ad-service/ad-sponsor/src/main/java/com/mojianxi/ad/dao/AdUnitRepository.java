package com.mojianxi.ad.dao;

import com.mojianxi.ad.entity.AdUnit;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * @Description TODO
 * @Author:liuxiaodong
 * @Date 2019/11/5 21:51
 * @Company dyld
 */
public interface AdUnitRepository extends JpaRepository<AdUnit,Long> {
    AdUnit findByPlanIdAndUnitName(Long planId,String unitName);
    List<AdUnit> findAllByUnitStatus(Integer unitStatus);
}

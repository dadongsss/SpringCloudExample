package com.mojianxi.ad.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.web.bind.annotation.CrossOrigin;

import javax.naming.Name;
import javax.persistence.*;
import java.util.Date;

/**
 * @Description TODO
 * @Author:liuxiaodong
 * @Date 2019/11/5 21:14
 * @Company dyld
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "ad_creative")
public class Creative {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id",nullable = false)
    private Long id;
    @Basic
    @Column(name = "name",nullable = false)
    private String name;
    @Basic
    @Column(name = "type",nullable = false)
    private Integer type;
    /*物料类型,如图片格式*/
    @Basic
    @Column(name = "materialType")
    private Integer materialType;
    @Basic
    @Column(name = "height",nullable = false)
    private Integer height;
    @Basic
    @Column(name = "width",nullable = false)
    private Integer width;
    @Basic
    @Column(name = "size",nullable = false)
    private Long size;
    @Basic
    @Column(name = "duration",nullable = false)
    private Integer duration;
    @Basic
    @Column(name = "auditStatus",nullable = false)
    private Integer auditStatus;
    @Basic
    @Column(name = "user_id",nullable = false)
    private Long userId;
    @Basic
    @Column(name = "url",nullable = false)
    private String url;
    @Basic
    @Column(name = "create_time",nullable = false)
    private Date createTime;
    @Basic
    @Column(name = "update_time",nullable = false)
    private Date updateTime;

}

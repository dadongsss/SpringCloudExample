package com.mojianxi.ad.dao.unit_condition;

import com.mojianxi.ad.entity.unit_condition.CreativeUnit;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @Description TODO
 * @Author:liuxiaodong
 * @Date 2019/11/5 21:59
 * @Company dyld
 */
public interface CreativeUnitRepository extends JpaRepository<CreativeUnit,Long> {
}

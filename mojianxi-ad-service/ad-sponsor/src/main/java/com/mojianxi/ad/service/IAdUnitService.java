package com.mojianxi.ad.service;

import com.mojianxi.ad.exception.AdException;
import com.mojianxi.ad.vo.*;

/**
 * @Description TODO
 * @Author:liuxiaodong
 * @Date 2019/11/5 23:07
 * @Company dyld
 */
public interface IAdUnitService {
    AdUnitResponse createUnit(AdUnitRequest request) throws AdException;
    AdUnitKeywordResponse createUnitKeyword(AdUnitKeywordRequest request)throws AdException;
    AdUnitItResponse createUnitIt(AdUnitItRequest request)throws AdException;
    AdUnitDistrictResponse createUnitDistrict(AdUnitDistrictRequest request)throws AdException;

    CreativeUnitResponse createCreativeUnit(CreativeUnitRequest request)throws AdException;
}

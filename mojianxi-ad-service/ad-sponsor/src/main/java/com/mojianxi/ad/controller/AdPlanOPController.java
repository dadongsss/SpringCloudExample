package com.mojianxi.ad.controller;

import com.alibaba.fastjson.JSON;
import com.mojianxi.ad.entity.AdPlan;
import com.mojianxi.ad.exception.AdException;
import com.mojianxi.ad.service.IAdPlanService;
import com.mojianxi.ad.vo.AdPlanGetRequest;
import com.mojianxi.ad.vo.AdPlanRequest;
import com.mojianxi.ad.vo.AdPlanResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @Description TODO
 * @Author:liuxiaodong
 * @Date 2019/11/6 0:36
 * @Company dyld
 */
@Slf4j
@RestController
public class AdPlanOPController {
    private final IAdPlanService adPlanService;

    @Autowired
    public AdPlanOPController(IAdPlanService adPlanService) {
        this.adPlanService = adPlanService;
    }
    public AdPlanResponse createAdPlan(@RequestBody AdPlanRequest request)throws AdException{
        log.info("ad-sponsor:createAdPlan -> {}", JSON.toJSONString(request));
        return adPlanService.createAdPlan(request);
    }
    @PostMapping("/get/adPlan")
    public List<AdPlan> getAdPlansByIds(@RequestBody AdPlanGetRequest request)throws AdException{
        log.info("ad-sponsor:createAdPlan -> {}",JSON.toJSONString(request));
        return adPlanService.getAdPlansByIds(request);
    }
    @PostMapping("/update/adPlan")
    public AdPlanResponse updateAdPlan(@RequestBody AdPlanRequest request)throws AdException{
        log.info("ad-sponsor:createAdPlan -> {}",JSON.toJSONString(request));
        return adPlanService.updateAdPlan(request);
    }
    @DeleteMapping("/delete/adPlan")
    public void deleteAdPlan(@RequestBody AdPlanRequest request)throws AdException{
        log.info("ad-sponsor:createAdPlan -> {}",JSON.toJSONString(request));
        adPlanService.deleteAdPlan(request);
    }
}

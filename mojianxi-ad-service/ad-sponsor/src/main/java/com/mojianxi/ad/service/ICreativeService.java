package com.mojianxi.ad.service;

import com.mojianxi.ad.vo.CreateUserResponse;
import com.mojianxi.ad.vo.CreativeRequest;
import com.mojianxi.ad.vo.CreativeResponse;

/**
 * @Description TODO
 * @Author:liuxiaodong
 * @Date 2019/11/6 0:02
 * @Company dyld
 */
public interface ICreativeService {
    CreativeResponse createCreative(CreativeRequest request);
}

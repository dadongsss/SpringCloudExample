package com.mojianxi.ad.dao.unit_condition;

import com.mojianxi.ad.entity.unit_condition.AdUnitDistrict;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @Description TODO
 * @Author:liuxiaodong
 * @Date 2019/11/5 21:58
 * @Company dyld
 */
public interface AdUnitDistrictRepository extends JpaRepository<AdUnitDistrict,Long> {
}

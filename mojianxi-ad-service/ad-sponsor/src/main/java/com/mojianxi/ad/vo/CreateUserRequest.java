package com.mojianxi.ad.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.util.StringUtils;

/**
 * @Description TODO
 * @Author:liuxiaodong
 * @Date 2019/11/5 22:03
 * @Company dyld
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class CreateUserRequest {
    private String username;
    public boolean validate(){
        return !StringUtils.isEmpty(username);
    }
}

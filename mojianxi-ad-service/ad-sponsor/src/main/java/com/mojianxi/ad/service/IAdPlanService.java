package com.mojianxi.ad.service;

import com.mojianxi.ad.entity.AdPlan;
import com.mojianxi.ad.exception.AdException;
import com.mojianxi.ad.vo.AdPlanGetRequest;
import com.mojianxi.ad.vo.AdPlanRequest;
import com.mojianxi.ad.vo.AdPlanResponse;

import java.util.List;

/**
 * @Description TODO
 * @Author:liuxiaodong
 * @Date 2019/11/5 22:22
 * @Company dyld
 */
public interface IAdPlanService {
    /*创建推广计划*/
    AdPlanResponse createAdPlan(AdPlanRequest request) throws AdException;
    /*获取推广计划*/
    List<AdPlan> getAdPlansByIds(AdPlanGetRequest request)throws AdException;
    /*更新推广计划*/
    AdPlanResponse updateAdPlan(AdPlanRequest request)throws AdException;
    /*删除推广计划*/
    void deleteAdPlan(AdPlanRequest request)throws AdException;
}

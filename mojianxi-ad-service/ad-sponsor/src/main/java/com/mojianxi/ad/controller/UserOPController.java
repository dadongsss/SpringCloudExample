package com.mojianxi.ad.controller;

import com.alibaba.fastjson.JSON;
import com.mojianxi.ad.exception.AdException;
import com.mojianxi.ad.service.IUserService;
import com.mojianxi.ad.vo.CreateUserRequest;
import com.mojianxi.ad.vo.CreateUserResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Description TODO
 * @Author:liuxiaodong
 * @Date 2019/11/6 0:31
 * @Company dyld
 */
@Slf4j
@RestController
public class UserOPController {
    private final IUserService userService;
    @Autowired
    public UserOPController(IUserService userService) {
        this.userService = userService;
    }
    @PostMapping("/create/user")
    public CreateUserResponse createUser(@RequestBody CreateUserRequest request)throws AdException{
        log.info("ad-sponsor:createUser -> {}", JSON.toJSONString(request));
        return userService.createUser(request);
    }
}

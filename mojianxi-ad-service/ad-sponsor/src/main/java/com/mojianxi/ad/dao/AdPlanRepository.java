package com.mojianxi.ad.dao;

import com.mojianxi.ad.entity.AdPlan;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * @Description TODO
 * @Author:liuxiaodong
 * @Date 2019/11/5 21:44
 * @Company dyld
 */
public interface AdPlanRepository extends JpaRepository<AdPlan,Long> {
    AdPlan findByIdAndUserId(Long id,Long userId);
    List<AdPlan> findAllByIdInAndUserId(List<Long> ids,Long userId);
    AdPlan findByUserIdAndPlanName(Long userId,String planName);
    List<AdPlan> findAllByPlanStatus(Integer status);
}

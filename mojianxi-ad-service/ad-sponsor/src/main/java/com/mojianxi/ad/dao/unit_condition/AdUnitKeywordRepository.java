package com.mojianxi.ad.dao.unit_condition;

import com.mojianxi.ad.entity.unit_condition.AdUnitKeyword;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @Description TODO
 * @Author:liuxiaodong
 * @Date 2019/11/5 21:56
 * @Company dyld
 */
public interface AdUnitKeywordRepository extends JpaRepository<AdUnitKeyword,Long> {
}

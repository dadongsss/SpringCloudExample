package com.mojianxi.ad.service.impl;

import com.mojianxi.ad.dao.CreativeRepository;
import com.mojianxi.ad.entity.Creative;
import com.mojianxi.ad.service.ICreativeService;
import com.mojianxi.ad.vo.CreativeRequest;
import com.mojianxi.ad.vo.CreativeResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @Description TODO
 * @Author:liuxiaodong
 * @Date 2019/11/6 0:10
 * @Company dyld
 */
@Service
public class CreativeServiceImpl implements ICreativeService {
    private final CreativeRepository creativeRepository;

    @Autowired
    public CreativeServiceImpl(CreativeRepository creativeRepository) {
        this.creativeRepository = creativeRepository;
    }

    @Override
    public CreativeResponse createCreative(CreativeRequest request) {
        Creative creative=creativeRepository.save(request.convertToEntity());
        return new CreativeResponse(creative.getId(),creative.getName());
    }
}

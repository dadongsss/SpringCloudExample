package com.mojianxi.ad.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @Description TODO
 * @Author:liuxiaodong
 * @Date 2019/11/6 0:09
 * @Company dyld
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class CreativeResponse {
    private Long id;
    private String name;
}

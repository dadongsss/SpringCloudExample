package com.mojianxi.ad.dao;

import com.mojianxi.ad.entity.AdUser;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @Description TODO
 * @Author:liuxiaodong
 * @Date 2019/11/5 21:42
 * @Company dyld
 */
public interface AdUserRepository extends JpaRepository<AdUser,Long> {
    AdUser findByUsername(String username);
}

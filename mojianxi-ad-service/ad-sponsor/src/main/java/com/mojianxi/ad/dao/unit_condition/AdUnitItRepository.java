package com.mojianxi.ad.dao.unit_condition;

import com.mojianxi.ad.entity.unit_condition.AdUnitIt;
import org.springframework.data.jpa.repository.JpaRepository;

import java.time.chrono.JapaneseChronology;

/**
 * @Description TODO
 * @Author:liuxiaodong
 * @Date 2019/11/5 21:57
 * @Company dyld
 */
public interface AdUnitItRepository extends JpaRepository<AdUnitIt,Long> {
}

package com.mojianxi.ad.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @Description TODO
 * @Author:liuxiaodong
 * @Date 2019/11/5 23:11
 * @Company dyld
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class AdUnitResponse {
    private Long id;
    private String unitName;
}

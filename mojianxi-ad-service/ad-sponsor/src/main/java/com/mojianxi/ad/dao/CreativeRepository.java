package com.mojianxi.ad.dao;

import com.mojianxi.ad.entity.Creative;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @Description TODO
 * @Author:liuxiaodong
 * @Date 2019/11/5 21:54
 * @Company dyld
 */
public interface CreativeRepository extends JpaRepository<Creative,Long> {

}

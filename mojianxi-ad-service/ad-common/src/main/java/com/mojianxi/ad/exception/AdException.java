package com.mojianxi.ad.exception;

/**
 * @Description TODO
 * @Author:liuxiaodong
 * @Date 2019/11/5 14:15
 * @Company dyld
 */
public class AdException extends Exception{
    public AdException(String message) {
        super(message);
    }

}

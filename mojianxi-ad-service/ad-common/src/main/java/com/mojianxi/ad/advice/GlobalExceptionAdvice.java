package com.mojianxi.ad.advice;

import com.mojianxi.ad.exception.AdException;
import com.mojianxi.ad.vo.CommonResponse;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import javax.servlet.http.HttpServletRequest;

/**
 * @Description TODO
 * @Author:liuxiaodong
 * @Date 2019/11/5 14:17
 * @Company dyld
 */
@RestControllerAdvice
public class GlobalExceptionAdvice {
    @ExceptionHandler(value = AdException.class)
    public CommonResponse<String> handlerAdException(HttpServletRequest req, AdException ex){
        CommonResponse<String> response=new CommonResponse<>(-1,"服务器错误");
        response.setData(ex.getMessage());
        return response;
    }
}

package com.mojianxi.ad.index.interest;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @Description TODO
 * @Author:liuxiaodong
 * @Date 2019/11/6 3:49
 * @Company dyld
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class UnitItObject {
    private Long unitId;
    private String itTag;
}

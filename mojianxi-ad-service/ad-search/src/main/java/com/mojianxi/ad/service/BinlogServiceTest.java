package com.mojianxi.ad.service;

import com.github.shyiko.mysql.binlog.BinaryLogClient;
import com.github.shyiko.mysql.binlog.event.DeleteRowsEventData;
import com.github.shyiko.mysql.binlog.event.EventData;
import com.github.shyiko.mysql.binlog.event.UpdateRowsEventData;
import com.github.shyiko.mysql.binlog.event.WriteRowsEventData;

/**
 * @Description TODO
 * @Author:liuxiaodong
 * @Date 2019/11/6 12:48
 * @Company dyld
 */
public class BinlogServiceTest {
    public static void main(String[] args) throws Exception {
        BinaryLogClient client=new BinaryLogClient("localhost",3306,"root","123456");
//        client.setBinlogFilename();
//        client.setBinlogPosition();
        client.registerEventListener(event -> {
            EventData data=event.getData();
            if(data instanceof UpdateRowsEventData){
                System.out.println("update-------------");
                System.out.println(data.toString());
            }else if(data instanceof WriteRowsEventData) {
                System.out.println("write---------------");
                System.out.println(data.toString());
            }else if(data instanceof DeleteRowsEventData){
                System.out.println("delete--------------");
                System.out.println(data.toString());
            }
        });
        client.connect();
    }
}

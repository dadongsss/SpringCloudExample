package com.mojianxi.ad.mysql.listener;

import com.mojianxi.ad.mysql.dto.BinlogRowData;

/**
 * @Description TODO
 * @Author:liuxiaodong
 * @Date 2019/11/6 13:34
 * @Company dyld
 */
public interface Ilistener {

    void register();

    void onEvent(BinlogRowData eventData);
}

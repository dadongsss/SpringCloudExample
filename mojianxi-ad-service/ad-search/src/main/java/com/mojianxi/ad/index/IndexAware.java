package com.mojianxi.ad.index;

/**
 * @Description TODO
 * @Author:liuxiaodong
 * @Date 2019/11/6 3:00
 * @Company dyld
 */
public interface IndexAware<K,V> {
    V get(K key);
    void add(K key,V value);
    void update(K key,V value);
    void delete(K key,V value);
}

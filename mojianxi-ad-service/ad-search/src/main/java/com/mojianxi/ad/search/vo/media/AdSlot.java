package com.mojianxi.ad.search.vo.media;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * @Description TODO
 * @Author:liuxiaodong
 * @Date 2019/11/6 17:34
 * @Company dyld
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class AdSlot {

    // 广告位编码
    private String adSlotCode;

    // 流量类型
    private Integer positionType;

    // 宽和高
    private Integer width;
    private Integer height;

    // 广告物料类型: 图片, 视频
    private List<Integer> type;

    // 最低出价
    private Integer minCpm;
}


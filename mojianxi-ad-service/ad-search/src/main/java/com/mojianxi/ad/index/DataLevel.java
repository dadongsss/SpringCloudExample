package com.mojianxi.ad.index;

import lombok.Getter;

/**
 * @Description TODO
 * @Author:liuxiaodong
 * @Date 2019/11/6 14:15
 * @Company dyld
 */
@Getter
public enum DataLevel {

    LEVEL2("2", "level 2"),
    LEVEL3("3", "level 3"),
    LEVEL4("4", "level 4");

    private String level;
    private String desc;

    DataLevel(String level, String desc) {
        this.level = level;
        this.desc = desc;
    }
}

package com.mojianxi.ad.client;

import com.mojianxi.ad.client.vo.AdPlan;
import com.mojianxi.ad.client.vo.AdPlanGetRequest;
import com.mojianxi.ad.vo.CommonResponse;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @Description TODO
 * @Author:liuxiaodong
 * @Date 2019/11/6 2:44
 * @Company dyld
 */
@Component
public class SponsorClientHystrix  implements SponsorClient{
    @Override
    public CommonResponse<List<AdPlan>> getAdPlans(AdPlanGetRequest request) {
        return new CommonResponse<>(-1,"eureka-client-ad-sponsor error");
    }
}

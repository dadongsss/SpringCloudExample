package com.mojianxi.ad.index.adplan;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * @Description TODO
 * @Author:liuxiaodong
 * @Date 2019/11/6 3:03
 * @Company dyld
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class AdPlanObject {
    private Long planId;
    private Long userId;
    private Integer planStatus;
    private Date startDate;
    private Date endDate;
    public void update(AdPlanObject newPlanObject){
        if(null!=newPlanObject.getPlanId()){
            this.planId=newPlanObject.getPlanId();
        }
        if(null!=newPlanObject.getUserId()){
            this.userId=newPlanObject.getUserId();
        }
        if(null!=newPlanObject.getPlanStatus()){
            this.planStatus=newPlanObject.getPlanStatus();
        }
         if(null!=newPlanObject.getStartDate()){
             this.startDate=newPlanObject.getStartDate();
         }
         if(null!=newPlanObject.getEndDate()){
             this.endDate=newPlanObject.getEndDate();
         }
    }
}

package com.mojianxi.ad.mysql.constant;


import com.github.shyiko.mysql.binlog.event.EventType;

/**
 * @Description TODO
 * @Author:liuxiaodong
 * @Date 2019/11/6 10:18
 * @Company dyld
 */
public enum OpType {
    ADD,
    UPDATE,
    DELETE,
    OTHER;

    public static OpType to(EventType eventType) {

        switch (eventType) {
            case EXT_WRITE_ROWS:
                return ADD;
            case EXT_UPDATE_ROWS:
                return UPDATE;
            case EXT_DELETE_ROWS:
                return DELETE;

            default:
                return OTHER;
        }
    }
}

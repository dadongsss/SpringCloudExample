package com.mojianxi.ad.index.adunit;

/**
 * @Description TODO
 * @Author:liuxiaodong
 * @Date 2019/11/6 3:44
 * @Company dyld
 */
public class AdUnitConstants {
    public static class POSITION_TYPE {

        public static final int KAIPING = 1;
        public static final int TIEPIAN = 2;
        public static final int TIEPIAN_MIDDLE = 4;
        public static final int TIEPIAN_PAUSE = 8;
        public static final int TIEPIAN_POST = 16;
    }
}

package com.mojianxi.ad.search.vo.feature;

/**
 * @Description TODO
 * @Author:liuxiaodong
 * @Date 2019/11/6 17:42
 * @Company dyld
 */
public enum FeatureRelation {

    OR,
    AND
}


package com.mojianxi.ad.search;

import com.mojianxi.ad.search.vo.SearchRequest;
import com.mojianxi.ad.search.vo.SearchResponse;

/**
 * @Description TODO
 * @Author:liuxiaodong
 * @Date 2019/11/6 17:30
 * @Company dyld
 */
public interface ISearch {
    SearchResponse fetchAds(SearchRequest request);
}

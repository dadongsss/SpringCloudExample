package com.mojianxi.ad.mysql.dto;

import com.mojianxi.ad.mysql.constant.OpType;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @Description TODO
 * @Author:liuxiaodong
 * @Date 2019/11/6 13:09
 * @Company dyld
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class TableTemplate {
    private String tableName;
    private String level;
    private Map<OpType, List<String>> opTypeFieldSetMap=new HashMap<>();
    /*字段名索引到字段映射*/
    private Map<Integer,String> posMap=new HashMap<>();
}

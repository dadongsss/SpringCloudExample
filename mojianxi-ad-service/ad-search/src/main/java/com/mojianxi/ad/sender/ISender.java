package com.mojianxi.ad.sender;

import com.mojianxi.ad.mysql.dto.MySqlRowData;

/**
 * @Description TODO
 * @Author:liuxiaodong
 * @Date 2019/11/6 13:52
 * @Company dyld
 */
public interface ISender {
    void sender(MySqlRowData rowData);
}

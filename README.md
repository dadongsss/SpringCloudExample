### SpringCloud广告系统案例

#### 广告系统准备工作与概览

###### 使用的技术

* JDK1.8
* Mysql8.0
* Maven3
* SpringCloud：Eureka、Zuul、Feign、SpringBoot2.2
* Kafka2.1.0：安装使用、术语解释、原生API

###### 广告系统概览

* 实现的功能
  * 广告主的广告投放：推广计划、推广单元
  * 媒体方的广告曝光：展示广告的媒介(网页、手机)、广告计费(CPM千次广告展现、CPT按时间计费、CPC点击类型收费)
* 一个完整的广告系统包含的子系统：广告投放系统、广告检索系统、曝光监测系统、扣费系统、报表系统
* 可以怎样扩展当前实现的广告系统：更多的维度、用户画像、AI
* 广告系统的架构
  * 广告主-广告投放系统-推广限制、广告创意
    * Mysql-全量广告数据文件：广告计划数据、推广限制数据、广告创意数据
    * 广告计费方式：CPM、CPC、CPT
  * 广告检索系统-广告数据索引(Slave检索全量广告文件)、广告检索服务
    * 地铁通道大屏、网页边栏、App开屏
  * 图示：![1572896866797](C:\Users\Administrator.DESKTOP-NBB06J4\AppData\Roaming\Typora\typora-user-images\1572896866797.png)

#### 广告系统通用模块与系统骨架

###### 广告系统骨架开发

* Maven

  * 基本样式
    * POM代表工程对象模型。它是使用Maven工作时的基本组件，是xml格式
    * POm包含了关于工程的各种配置细节信息，Maven使用这些信息构建工程
    * maven坐标
      - groupId：这是工程组的标识。它在一个组织或者项目中通常是唯一的。例如，一个卡包项目拥有所有的和卡包相关的项目
      - artifactId：这是工程的标识。它通常是工程的名称。例如，用户卡包。groupId和artifactId一起定义了artifact在仓库中的位置
      - version：这是工程的版本号。在artifact的仓库中，它用来区分不同的版本。
  * 常用命令
    * mvn -v：查看maven的版本，也用来检查是否安装成功
    * mvn compile：编译，将java源文件编译成class文件
    * mvn test：执行test目录下的测试用例
    * mvn package：打包，将java工程打包成jar包
    * mvn clean：清理环境，清除target文件夹
    * mvn install：安装，将当前项目安装到maven本地仓库中
  * 依赖特性
    * 传递依赖：如果我们的项目引用了一个jar包，而该jar包又引用了其它jar包。那么在默认情况下，项目编译时，maven会把直接引用和间接引用的jar包都下载到本地
    * 排除依赖：如果我们只想下载直接引用的jar包，那么需要在pom.xml中配置exclusions
    * 依赖冲突
      * 说明：若项目中多个jar同时引用了相同的jar时，会产生依赖冲突，但maven采用了两种避免冲突的策略，因此在maven中是不存在依赖冲突的
      * 短路优先：引用路径短的优先
      * 声明优先：若引用路径长度相同时，在pom.xml中谁先被声明就使用谁
      * 不同版本的冲突maven不会处理，需要自己排除
  * 多模块组合
    * 根项目是壳子，packaging类型是pom配置父模块；modules聚合子模块；父模块使用dependencyManagement统一管理依赖包；子模块需要在pom中使用parent标签声明父模块

* 项目创建与eureka模块

  * idea创建父项目删除src目录，在pom添加packaging；配置主要引用包和父项目基本配置

  * 创建eureka模块，配置yum启动查看信息：

    ```
    #发现服务
    client:
      service-url:
        defaultZone: http://server1:8000/eureka/,http://server3:8001/eureka/
    ```

  * 启动多节点eureka并互相注册，形成多节点eureka-server集群

  * mvn clean package -Dmaven.test.skip=true -U在命令行打包

  * 在命令行输入java -jar ad-eureka-1.0-SNAPSHOT.jar --spring.profiles.active=server1分别运行多个eureka应用

* 微服务架构和应用场景-微服务架构的两种方式

  * 点对点的方式：服务之间直接调用，每个微服务都开放Rest API,并调用其它微服务接口，这种方式不易维护![1572929778028](C:\Users\Administrator.DESKTOP-NBB06J4\AppData\Roaming\Typora\typora-user-images\1572929778028.png)
  * API网关方式：业务接口通过API网关暴露，是所有客户端接口的唯一入口。微服务之间的通信也通过API网关![1572930046936](C:\Users\Administrator.DESKTOP-NBB06J4\AppData\Roaming\Typora\typora-user-images\1572930046936.png)

* Zuul网关应用

  * zuul的生命周期![1572930153104](C:\Users\Administrator.DESKTOP-NBB06J4\AppData\Roaming\Typora\typora-user-images\1572930153104.png)
  * 创建zuul项目ad-gateway配置yml，注册到eureka
  * 创建前置过滤器PreRequestFilter，注册到容器，实现四个方法
    * filterType：定义filter类型-pre、route、post、error
    * filterOrder：定义filter执行顺序，数字越小顺序越高
    * shouldFilter：是否需要执行过滤器，定义验证
    * run：执行的具体操作
  * 创建日志打印过滤器AccessLogFilter

###### 微服务通用模块

* 通用模块的作用：
  * 通用代码定义，配置定义
  * 统一的响应处理
    * 通过控制器增强(Rest)ControllerAdvice和ResponseBodyAdvice包装响应结果
  * 统一的异常处理
    1. 不直接展示错误，对用户友好
    2. 异常分类，便于排查问题，DEBUG
    3. 降低业务代码中对异常处理的耦合
* 创建业务层模块,并在业务层模块下创建ad-common通用子模块
  * 统一响应
    * 定义统一响应类CommonResponse
    * 定义CommonResponseDataAdvice实现统一拦截，更改请求返回格式
      - supports：根据条件判断是否拦截，这里对自定义注解IgnoreResponseAdvice做处理
      - beforeBodyWrite：写入响应前做一些操作，这里处理统一响应
    * 自定义注解IgnoreResponseAdvice，来忽略不需要统一响应的拦截
  * 统一异常处理
    * 通过异常类AdException和控制器增强GlobalExceptionAdvice处理异常
  * 通用配置定义-消息转换器
    * 消息转换器的目标是将http输入的数据格式和java对象数据互转
    * 通过WebConfiguration来配置通用消息转换

#### 广告投放系统开发

###### 广告投放系统常用功能与数据库

* SpringIOC原理分析：Spring通过IOC控制反转架起使用的桥梁，IOC是Spring最重要最核心的思想。![1572935804343](C:\Users\Administrator.DESKTOP-NBB06J4\AppData\Roaming\Typora\typora-user-images\1572935804343.png)
* SpringMVC模块分析：mvc是SpringBoot的一部分![1572935928816](C:\Users\Administrator.DESKTOP-NBB06J4\AppData\Roaming\Typora\typora-user-images\1572935928816.png)
* SpingBoot常用功能特性
  * SpringBoot启动入口
  * 容器启动后执行的操作
  * Profile环境配置
  * 配置信息封装成实体类
  * 定时任务
* 表分析
  * 四个概念：用户账户、推广计划、推广单元、创意
  * 推广单元：关键词限制、地域限制、兴趣限制
  * 广告投放系统层级关系
    * 最高层级-推广账户：用户账户
    * 第二层级-推广计划：产品A推广计划、产品B推广计划
    * 第三层级-推广单元：关键词推广单元、地域推广单元、兴趣推广单元、人群推广单元
      * 创意：文字创意、图片创意、视屏创意、GIF创意
  * 广告投放系统表概况
    * 用户账户：username、token、user_status、create_time、update_time
    * 推广计划：user_id、plan_name、plane_status、start_date、end_date、create_time、update_time
    * 推广单元表：plan_id(关联推广计划id)、unit_name、unit_status、position_type(广告位类型)、budget(预算)、create_time、update_time
    * 关键词限制：unit_id、keyword
    * 地域限制：unit_id、province、city
    * 兴趣限制：unit_id、it_tag(兴趣标签)
    * 创意：name(创意名称)、type(物料类型)、material_type(物料子类型)、height(高度)、width(宽度)、size(物料大小)、duration(持续时长)、audit_status(审核状态)、user_id、url、create_time、update_time
    * 创意与推广单元关联：creative_id(关联创意id)、unit_id
* 创建广告投放系统子模块ad-sponsor
  * 创建模块定义实体类相关与dao层数据查询接口

###### 广告系统服务开发与网关配置

* 服务模块实现
  * 封装请求参数、封装返回数据，service层实现数据操作
  * Controller控制层实现
* 网关配置
  * 在网关项目配置项目访问前缀与子模块的路径

#### 广告检索系统开发

###### 子模块创建与广告微服务调用

* 创建子模块，在pom文件引入类库
* 在yml配置数据库、eureka、feign、management等信息
* 微服务调用![1572977555144](C:\Users\Administrator.DESKTOP-NBB06J4\AppData\Roaming\Typora\typora-user-images\1572977555144.png)
* 通过RestTemplate用Ribbon来调用广告投放系统
* 使用feign实现更简单的调用其它模块
* 断路器定义，实现服务降级

###### 广告数据索引的设计与实现

* 数据索引设计：
  * 正向索引：通过唯一键/主键生成与对象的映射关系
  * 倒排索引：也被称为反向索引，是一种索引方法，它的设计是为了存储在全文搜索下某个单词在一个文档或一组文档中存储位置的映射。是在文档检索系统中最常用的数据结构
    * 倒排索引在广告系统中的应用是对各个维度限制的整理![1572980028440](C:\Users\Administrator.DESKTOP-NBB06J4\AppData\Roaming\Typora\typora-user-images\1572980028440.png)
* 广告数据索引维护
  * 全量索引：检索系统在启动时一次性读取当前数据库中的所有数据，建立索引
  * 增量索引：系统运行过程中，监控数据库变化，即增量，实时加载更新，构建索引
  * 图示：![1572980323623](C:\Users\Administrator.DESKTOP-NBB06J4\AppData\Roaming\Typora\typora-user-images\1572980323623.png)
* 关键词、地域、兴趣、创意的索引对象定义与服务实现
* 索引服务类缓存实现

###### Mysql Binlog

* binlog：二进制日志，记录对数据发生或潜在发生更改的sql语句，并以二进制形式保存在磁盘中。

* Binlog的两个主要作用：复制、恢复和审计

* binlong相关变量

  * log_bin：binlog开关-show variables like ‘log_bin’
  * binlog_format：binlog的日志格式-show variables like 'binlog_format'

* 开启MySQLlogbin在配置文件添加如下代码

  * ```
    server_id=1918
    log_bin = mysql-bin
    binlog_format = ROW
    ```

* binlog日志的三种格式

  * ROW：仅保存记录被修改细节，不记录sql语句上下文相关信息
  * STATEMENT：每一条会修改数据的sql都会记录在binlog中
  * MIXED：以上两种level的混合

* 管理binlog相关的sql语句

  * show master logs：查看所有binlog的日志列表
  * show master status：查看最后一个binlog日志编号的名称，以及最后一个事件结束的位置
  * flush logs：刷新binlog，此刻开始产生一个新编号的binlog日志文件
  * reset master：清空所有binlog日志
  * 查看binlog相关的sql语句
    * show binlog events -查看第一个binlog日志
    * show binlog events in 'binlog.00030'-查看指定的binlog日志
    * show binlog events in 'binlog.00030' from 931-从指定的位置开始，查看指定的binlog日志
    * show binlog events in 'binlog.00030' from 931 limit 2-从指定的位置开始，查看指定的binlog日志,限制查询的条数
    * show binlog events in 'binlog.00030' from 931 limit 1,2-从指定的位置开始，带有偏移，查看指定的binlog日志，限制查询的条数
  * binlog中的几个Event_type
    * QUERY_EVENT：与数据无关的操作
    * TYBLE_MAP_EVENT：记录下一个操作所对应的表信息，存储了数据库名和表名
    * XID_EVENT：标记事务提交
    * WRITE_ROWS_EVENT：插入数据操作
    * UPDATE_ROWS_EVENT：更新数据操作
    * DELETE_ROWS_EVENT：删除数据操作

* 在项目中使用shyiko开源工具对mysql-binlog进行监听与解析

  * 定义测服务对mysql操作进行监听
  * 使用模板文件对binlog进行监听解析

* 增量数据投递并将增量数据注册到kafka

* 广告检索服务

  * 广告检索服务功能介绍![1573032393972](C:\Users\Administrator.DESKTOP-NBB06J4\AppData\Roaming\Typora\typora-user-images\1573032393972.png)

#### kafka安装与使用

###### kafka介绍

* 消息系统分点对点消息系统和发布订阅消息系统
* kafka术语
  * ![1573035840460](C:\Users\Administrator.DESKTOP-NBB06J4\AppData\Roaming\Typora\typora-user-images\1573035840460.png)

###### Kafka安装与使用

* 安装
* 命令介绍
  * zookeeper-bin/zookeeper-server-start.sh -daeomon config/zookeeper.properties
  * kafka-server-bin/kafka-server.sh config/server.properties
  * create topic-bin/kafka-topics.sh --create --zookeeper localhost:2181 --relication-factor 1 -- partitions 1 --topic test
  * topic list-bin/kafka-topics.sh --list --zookeer localhost:2181
  * producer-bin/kafka-console-producer.sh --broker-list localhost:9092 --topic test
  * consumer-bin/kafka-console-consumer.sh --bootstrap-server localhost:9092 --topic test --from-beginning
  * topic info-bin/kafka-topics.sh --describe --zookeeper localhost:2128 --topic test
* 命令在win下使用
  * .\bin\windows\zookeeper-server-start.bat -daemon config/zookeeper.properties：启动zookeeper
  * .\bin\windows\kafka-server-start.bat .\config\server.properties：启动kafka
  * .\bin\windows\kafka-topics.bat --create --zookeeper localhost:2181 --replication-factor 1 --partitions 1 --topic mojianxi-kafka-demo：创建topic
  * .\bin\windows\kafka-topics.bat --list --zookeeper localhost:2181：查询topic
  * .\bin\windows\kafka-console-producer.bat --broker-list localhost:9092 --topic mojianxi_ad_demo_a：启动producer
  * .\bin\windows\kafka-console-consumer.bat --bootstrap-server localhost:9092 --topic mojianxi_ad_test_x from-beginning：启动consumer
* 消费消息的方式：自动提交位移、手动提交当前位移、手动异步提交当前位移、手动异步提交当前位移带回调、混合同步与异步提交位移

###### 总结与测试

* Hystrix熔断效率不高单独使用较少，一般与Feign结合使用，当feign抛出异常，回调熔断实现回退
* 新建Dashboard子模块，配置yml并新建入口application
* 检索系统测试用例